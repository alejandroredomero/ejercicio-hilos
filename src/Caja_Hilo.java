import java.util.concurrent.Semaphore;

public class Caja_Hilo implements Runnable{
    public Caja caja;
    public Semaphore semaforo;
    public String cliente;
    public int productos;
    Thread t;

    public Caja_Hilo(Caja caja, Semaphore semaforo, String cliente, int productos) {
        this.caja = caja;
        this.semaforo = semaforo;
        this.cliente = cliente;
        this.productos = productos;
        Thread t = new Thread(this);
        t.start();
    }


    @Override
    public void run() {
        caja.procesarCompra(cliente, productos, semaforo);
    }

}
