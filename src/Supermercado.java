import java.util.concurrent.Semaphore;

public class Supermercado {
    public static void main(String[] args) {

        Semaphore semaforo = new Semaphore(2);
        Caja caja1 = new Caja("Caja 1");
        Caja caja2 = new Caja("Caja 2");
        Caja caja3 = new Caja("Caja 3");

        Caja_Hilo cajaHilo1 = new Caja_Hilo(caja1, semaforo, "Lucas", 5);
        Caja_Hilo cajaHilo2 = new Caja_Hilo(caja2, semaforo, "Daniel", 1);
        Caja_Hilo cajaHilo3 = new Caja_Hilo(caja3, semaforo, "Vitori", 4);
    }
}


