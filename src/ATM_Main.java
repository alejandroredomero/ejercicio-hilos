import java.util.concurrent.Semaphore;

public class ATM_Main {
    public static void main(String[] args) {

        Semaphore semaforo = new Semaphore(3);
        ATM atm1 = new ATM("ATM 1", semaforo);
        ATM atm2 = new ATM("ATM 2", semaforo);
        ATM atm3 = new ATM("ATM 3", semaforo);
        ATM atm4 = new ATM("ATM 4", semaforo);
        ATM atm5 = new ATM("ATM 5", semaforo);

        atm1.start();
        atm2.start();
        atm3.start();
        atm4.start();
        atm5.start();
    }
}
