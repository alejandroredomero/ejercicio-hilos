import java.util.concurrent.Semaphore;

public class ATM extends Thread{
    public String nombre;
    public Semaphore semaforo;


    public ATM(String nombre, Semaphore semaforo) {
        this.nombre = nombre;
        this.semaforo = semaforo;
    }
    @Override
    public void run() {

        int tiempo_de_uso = (int) (Math.random() * 8 + 3); // genera un número aleatorio entre 3 y 10 segundos
        try {
            // Adquirimos los permisos del sefamoro
            semaforo.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(nombre + " se está utilizando");

        for (int i = 1; i <= tiempo_de_uso; i++) {
            try {
                sleep(1000); // Esto hace que espere 1 segundo
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(nombre + " se está utilizando");
        }
        semaforo.release(); // Sirve para liberar permiso del semáforo
        System.out.println(nombre + " libre");

    }

}
