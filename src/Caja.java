import java.util.concurrent.Semaphore;

public class Caja {
    public String nombre;

    public Caja(String nombre) {
        this.nombre = nombre;
    }

    public void procesarCompra(String cliente, int productos, Semaphore semaforo) {
        try {
            semaforo.acquire();
            System.out.println("La caja " + nombre + " recibe al cliente " + cliente);
            for (int i = 1; i <= productos; i++) {
                System.out.println("Caja " + nombre + " - Cliente: " + cliente + " - Producto " + i);
                int num = (int) (Math.random() * 10000);
                Thread.sleep(num);
            }
            System.out.println(nombre + " libre");
            semaforo.release();

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}


